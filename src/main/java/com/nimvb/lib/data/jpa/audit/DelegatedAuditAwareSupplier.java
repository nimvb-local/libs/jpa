package com.nimvb.lib.data.jpa.audit;

import java.util.function.Supplier;

public interface DelegatedAuditAwareSupplier<T> extends Supplier<T> {
}
