package com.nimvb.lib.data.jpa.config;

import com.nimvb.lib.data.jpa.audit.DelegatedAuditAware;
import com.nimvb.lib.data.jpa.audit.DelegatedAuditAwareSupplier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;

@Configuration
public class AuditableEntityConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public DelegatedAuditAwareSupplier<String> supplier() {
        return () -> null;
    }

    @Bean
    @ConditionalOnMissingBean
    public AuditorAware<String> delegatedAuditAwareBean(DelegatedAuditAwareSupplier<String> supplier){
        return new DelegatedAuditAware<>(supplier);
    }
}
