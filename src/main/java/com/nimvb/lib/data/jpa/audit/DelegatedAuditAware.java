package com.nimvb.lib.data.jpa.audit;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.AuditorAware;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;


@RequiredArgsConstructor
public class DelegatedAuditAware<T> implements AuditorAware<T> {

    private final DelegatedAuditAwareSupplier<T> supplier;


    @Override
    public Optional<T> getCurrentAuditor() {
        return Optional.ofNullable(supplier.get());
    }
}
