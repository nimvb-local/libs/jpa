package com.nimvb.lib.data.jpa.audit;

import com.nimvb.lib.data.jpa.ApplicationTestsConfiguration;
import com.nimvb.lib.data.jpa.context.model.CustomEntity;
import com.nimvb.lib.data.jpa.context.repository.EntityRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = {ApplicationTestsConfiguration.class
})

@DataJpaTest
public class AbstractAuditableEntityTest {

    @Autowired
    EntityRepository entityRepository;

    @Test
    void contextLoads() {
        CustomEntity entity = new CustomEntity();
        CustomEntity save = entityRepository.save(entity);
        Assertions.assertThat(save).isNotNull();
        Assertions.assertThat(save.getCreatedBy()).isNotNull();
        Assertions.assertThat(save.getModifiedAt()).isNotNull();
        Assertions.assertThat(save.getCreatedAt()).isNotNull();
        Assertions.assertThat(save.getModifiedAt()).isNotNull();
    }
}