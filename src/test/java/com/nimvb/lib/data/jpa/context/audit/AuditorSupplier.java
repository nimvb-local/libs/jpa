package com.nimvb.lib.data.jpa.context.audit;

import com.nimvb.lib.data.jpa.audit.DelegatedAuditAwareSupplier;
import org.springframework.stereotype.Component;

@Component
public class AuditorSupplier implements DelegatedAuditAwareSupplier<String> {
    @Override
    public String get() {
        return "Mr. who";
    }
}
