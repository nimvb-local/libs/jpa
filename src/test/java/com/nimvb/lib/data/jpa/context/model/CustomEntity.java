package com.nimvb.lib.data.jpa.context.model;

import com.nimvb.lib.data.jpa.audit.AbstractAuditableEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class CustomEntity extends AbstractAuditableEntity<String> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
}
