package com.nimvb.lib.data.jpa.context.repository;

import com.nimvb.lib.data.jpa.context.model.CustomEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface EntityRepository extends JpaRepository<CustomEntity,Long> {
}
