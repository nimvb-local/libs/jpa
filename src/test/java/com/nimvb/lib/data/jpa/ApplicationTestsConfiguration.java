package com.nimvb.lib.data.jpa;

import com.nimvb.lib.data.jpa.audit.AbstractAuditableEntity;
import com.nimvb.lib.data.jpa.audit.AbstractAuditableEntityTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//@ExtendWith(SpringExtension.class)
//@SpringBootTest
@ComponentScan({"com.nimvb.lib.data.jpa.*"})
@EnableAutoConfiguration
@SpringBootConfiguration
@EnableJpaAuditing
public class ApplicationTestsConfiguration {



//    @TestConfiguration
//    static class Configuration {
//
//        @javax.persistence.Entity
//        class Entity extends AbstractAuditableEntity<String> {
//
//            @Id
//            @GeneratedValue(strategy = GenerationType.IDENTITY)
//            private Long id;
//        }
//
//        @Repository
//        public interface EntityRepository extends JpaRepository<Entity, Long> {
//
//        }
//    }


    //    @Autowired
//    Configuration.EntityRepository repository;
    @Bean
    AbstractAuditableEntityTest bean() {
        return new AbstractAuditableEntityTest();
    }

}
